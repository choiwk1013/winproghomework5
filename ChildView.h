
// ChildView.h : CChildView 클래스의 인터페이스
//


#pragma once
#include "afxtempl.h"
#include "afx.h"


// CChildView 창

class CChildView : public CWnd
{
// 생성입니다.
public:
	CChildView();

// 특성입니다.
public:

// 작업입니다.
public:

// 재정의입니다.
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// 구현입니다.
public:
	virtual ~CChildView();

	// 생성된 메시지 맵 함수
protected:
	afx_msg void OnPaint();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
private:
	CArray<TCHAR, TCHAR> m_str;
public:
	afx_msg void OnModeFile();
	afx_msg void OnModeMem();
	afx_msg void OnUpdateModeFile(CCmdUI *pCmdUI);
	afx_msg void OnUpdateModeMem(CCmdUI *pCmdUI);
private:
	bool m_mem_mode;
public:
	afx_msg void OnCommandSave();
	afx_msg void OnCommandRecentLoad();
private:
	CArray<CString, CString&> m_file_list;
public:
	void GetSavedFileList();
private:
	int m_cur_file;
public:
	afx_msg void OnNextLoad();
	afx_msg void OnPrevLoad();
	void LoadFile(int pos);
	void SaveFile(int pos);
	void SaveMem();
	void LoadMem();
private:
	CMemFile m_mem_file;
public:
	afx_msg void OnCommandUpper();
};

