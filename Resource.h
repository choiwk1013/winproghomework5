//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// WinProgHomework5.rc에서 사용되고 있습니다.
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_WinProgHomeworkTYPE         130
#define ID_32771                        32771
#define ID_32772                        32772
#define ID_MODE_FILE                    32773
#define ID_MODE_MEM                     32774
#define ID_32775                        32775
#define ID_COMMAND_SAVE                 32776
#define ID_32777                        32777
#define ID_Menu                         32778
#define ID_COMMAND_RECENT_LOAD          32779
#define ID_32780                        32780
#define ID_32781                        32781
#define ID_NEXT_LOAD                    32782
#define ID_PREV_LOAD                    32783
#define ID_32788                        32788
#define ID_COMMAND_UPPER                32789

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        310
#define _APS_NEXT_COMMAND_VALUE         32790
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           310
#endif
#endif
