
// ChildView.cpp : CChildView 클래스의 구현
//

#include "stdafx.h"
#include "WinProgHomework5.h"
#include "ChildView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CChildView

CChildView::CChildView()
: m_mem_mode(false)
, m_cur_file(0)
{
}

CChildView::~CChildView()
{
}


BEGIN_MESSAGE_MAP(CChildView, CWnd)
	ON_WM_PAINT()
	ON_WM_CHAR()
	ON_COMMAND(ID_MODE_FILE, &CChildView::OnModeFile)
	ON_COMMAND(ID_MODE_MEM, &CChildView::OnModeMem)
	ON_UPDATE_COMMAND_UI(ID_MODE_FILE, &CChildView::OnUpdateModeFile)
	ON_UPDATE_COMMAND_UI(ID_MODE_MEM, &CChildView::OnUpdateModeMem)
	ON_COMMAND(ID_COMMAND_SAVE, &CChildView::OnCommandSave)
	ON_COMMAND(ID_COMMAND_RECENT_LOAD, &CChildView::OnCommandRecentLoad)
	ON_COMMAND(ID_NEXT_LOAD, &CChildView::OnNextLoad)
	ON_COMMAND(ID_PREV_LOAD, &CChildView::OnPrevLoad)
	ON_COMMAND(ID_COMMAND_UPPER, &CChildView::OnCommandUpper)
END_MESSAGE_MAP()



// CChildView 메시지 처리기

BOOL CChildView::PreCreateWindow(CREATESTRUCT& cs) 
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_CLIENTEDGE;
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW|CS_VREDRAW|CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW+1), NULL);

	return TRUE;
}

void CChildView::OnPaint() 
{
	CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.
	CFont font;
	font.CreatePointFont(150, _T("궁서"));
	dc.SelectObject(&font);

	CRect rect;
	GetClientRect(&rect);
	dc.DrawText(m_str.GetData(), m_str.GetSize(), &rect, DT_LEFT);
}



void CChildView::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if (nChar == _T('\b')) {
		if (m_str.GetSize() > 0) {
			m_str.RemoveAt(m_str.GetSize() - 1);
		}
	}
	else {
		m_str.Add(nChar);
	}

	Invalidate();

	CWnd::OnChar(nChar, nRepCnt, nFlags);
}


void CChildView::OnModeFile()
{
	m_mem_mode = false;
}


void CChildView::OnModeMem()
{
	m_mem_mode = true;
}


void CChildView::OnUpdateModeFile(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck(!m_mem_mode);
}


void CChildView::OnUpdateModeMem(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck(m_mem_mode);
}


void CChildView::OnCommandSave()
{
	if (!m_mem_mode) {
		GetSavedFileList();	//	파일을 불러오기전 파일 이름을 불러온다.
		SaveFile(m_file_list.GetSize());	
	}
	else {
		SaveMem();
	}

	Invalidate();
}


void CChildView::OnCommandRecentLoad()
{
	if (!m_mem_mode) {
		GetSavedFileList();	//	파일을 불러오기전 파일 이름을 불러온다.
		LoadFile(m_file_list.GetCount() - 1);
	}
	else {
		LoadMem();
	}

	Invalidate();
}


void CChildView::OnNextLoad()
{
	if (!m_mem_mode) {
		GetSavedFileList();	//	파일을 불러오기전 파일 이름을 불러온다.
		if (m_cur_file < m_file_list.GetCount() - 1) {
			m_cur_file++;
		}
		LoadFile(m_cur_file);
	}
	else {
		AfxMessageBox(_T("메모리 모드에서는 작동하지 않습니다."));
	}

	Invalidate();
}


void CChildView::OnPrevLoad()
{
	if (!m_mem_mode) {
		GetSavedFileList();	//	파일을 불러오기전 파일 이름을 불러온다.

		if (m_cur_file > 0) {
			m_cur_file--;
		}
		LoadFile(m_cur_file);
	}
	else {
		AfxMessageBox(_T("메모리 모드에서는 작동하지 않습니다."));
	}
	
	Invalidate();
}


void CChildView::OnCommandUpper()
{
	if (!m_mem_mode) {
		GetSavedFileList();	//	파일을 불러오기전 파일 이름을 불러온다.
		CString file_name;
		file_name.Format(_T("data%d.txt"), m_cur_file);

		CStdioFile file1;
		CFileException e;
		if (!file1.Open(file_name, CFile::modeRead, &e)) {
			e.ReportError();
			return;
		}

		CStdioFile file2;
		if (!file2.Open(_T("upper.txt"), CFile::modeWrite | CFile::modeCreate, &e)) {
			e.ReportError();
			return;
		}

		CString str;
		while (file1.ReadString(str)) {
			str.MakeUpper();
			file2.WriteString(str + '\n');
		}
	}
	else {
		AfxMessageBox(_T("메모리 모드에서는 작동하지 않습니다."));
	}
}



void CChildView::GetSavedFileList()
{
	m_file_list.RemoveAll();

	CFileFind finder;
	BOOL bWorking = finder.FindFile(_T("data*.txt"));
	while (bWorking) {
		bWorking = finder.FindNextFile();
		CString str = (LPCTSTR)finder.GetFileName();
		m_file_list.Add(str);
	}
}


void CChildView::LoadFile(int pos)
{
	GetSavedFileList();	//	파일을 불러오기전 파일 이름을 불러온다.

	//	가장 최근 파일을 파일 이름으로
	CString file_name = m_file_list.GetAt(pos);
	m_cur_file = pos;

	AfxMessageBox(file_name + _T(" 파일을 불러옵니다."));

	CFile file;
	CFileException e;
	if (!file.Open(file_name, CFile::modeRead, &e))
		e.ReportError();

	m_str.RemoveAll();

	TCHAR buf[10];
	int n;
	while ((n = file.Read(buf, sizeof(TCHAR)* 10)) > 0) {
		for (int i = 0; i < n / sizeof(TCHAR); i++) {
			m_str.Add(buf[i]);
		}
	}
}


void CChildView::SaveFile(int pos)
{
	GetSavedFileList();	//	저장하기 전 파일 목록을 불러온다.

	//	파일 이름을 생성한다.
	CString file_name;
	file_name.Format(_T("data%d.txt"), pos);

	CFile file;
	CFileException e;
	if (!file.Open(file_name, CFile::modeWrite | CFile::modeCreate, &e))
		e.ReportError();

	file.Write(m_str.GetData(), sizeof(TCHAR)* m_str.GetSize());

	GetSavedFileList();	//	저장한 후 파일 목록을 불러온다.

	m_str.RemoveAll();
	AfxMessageBox(file_name + _T(" 파일로 저장하였습니다."));
}


void CChildView::SaveMem()
{
	m_mem_file.SeekToBegin();
	UINT size = m_str.GetSize();
	m_mem_file.Write(&size, sizeof(UINT));	//	길이를 같이 저장
	m_mem_file.Write(m_str.GetData(), sizeof(TCHAR)* m_str.GetSize());
}


void CChildView::LoadMem()
{
	m_str.RemoveAll();
	m_mem_file.SeekToBegin();

	//	길이를 불러옴
	UINT size;
	m_mem_file.Read(&size, sizeof(UINT));

	for (int i = 0; i < size; i++) {
		TCHAR c;
		m_mem_file.Read(&c, sizeof(TCHAR));
		m_str.Add(c);
	}
}


